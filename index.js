const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');


const port = process.env.port || 4500;
const app = express();


mongoose.connect("mongodb+srv://admin_medes:admin169@cluster0.ovov6.mongodb.net/eCommerceAPI?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));

app.use(express.json());
app.use(cors());

app.use('/users', userRoutes);
app.use('/products', productRoutes);


app.listen(port, () => console.log(`Server is running at port ${port}`));

// https://calm-brook-85405.herokuapp.com/
