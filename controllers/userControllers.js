const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//For Signing Up/ Registration.
module.exports.registerUser = (req, res) => {

	User.find({})
	.then(users => {

		let searchUserName = users.filter(user => {
			return user['userName'] === req.body.userName
		})

		if(searchUserName.length === 0){

			const hashedPW = bcrypt.hashSync(req.body.password, 10)

		let newUser = new User({

		userName: req.body.userName,
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPW,
		mobileNo: req.body.mobileNo

		})
	
			newUser.save();
			res.send(newUser);
		} else {
			return res.status().send('The Username is already used. Please try again.')
		}
	})
	.catch(err => res.send(err));
}



//For Checking if Email Exists.
module.exports.checkEmailExists = (req,res) => {
	User.findOne({email: req.body.email})
	.then(result => {
		if(result === null){
			return res.send("Email is available.");
		} else {
			return res.send("Email is already registered.")
		}
	})
	.catch(err => res.send(err));
};


//For Regular User Log-In.
module.exports.loginUser = (req, res) => {
	console.log(req.body);
	User.findOne({email: req.body.email})
		.then(foundUser => {
			if(foundUser === null){
				return res.send("User does not exist");
			} else {
				const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
					if(isPasswordCorrect){
			 			return res.send({accessToken: auth.createAccessToken(foundUser)})
					} else {
			 			return res.send("Password is incorrect.")
			 		}
			}
		})
		.catch(err => res.send(err));
};


//For Retrieving User Details.
module.exports.getUserDetails = (req, res) => {
	console.log(req.user)
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


//For Updating a Regular User to Admin.
module.exports.updateUserAsAdmin = (req, res) => {
	console.log(req.user.id);
	console.log(req.params.id);
		let updates = {
			isAdmin: true
		};
	User.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};


//For Updating an Admin to User.
module.exports.updateAdminToUser = (req, res) => {
	console.log(req.user.id);
	console.log(req.params.id);
		let updates = {
			isAdmin: false
		};
	User.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};


//For Retrieving All Users (Admin Only).
module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};




//Route for Creating an Order (Regular User Only).
module.exports.createOrder = async (req, res) => {

	// log info to check
	console.log(req.user.id);
	console.log(req.body.productId);
	console.log(req.body.totalAmount);

	// check if user is an admin
	if (req.user.isAdmin) {
		return res.send("Action Forbidden for Admin.");
	};

	// isUserUpdated
	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {

		// log info to check
		console.log(user);

		// create an object which will contain the orderId of the product ordered
		let newOrder = {
			userId : req.user.id,
			productId : req.body.productId,
			totalAmount : req.body.totalAmount
		};
		user.orders.push(newOrder);

		// save
		return user.save()
		.then(user => true)
		.catch(err => err.message);
	});

	// check if user is updated
	if (isUserUpdated !== true) {
		return res.send({message : isUserUpdated});
	}

	// isProductUpdated
	let isProductUpdated = await Product.findById(req.body.productId)
	.then(product => {

		// log info to check
		console.log(product);

		// create an object which will contain the userId of the user who made the order
		let orderer = {
			userId : req.user.id,
			productId : req.body.productId,
			totalAmount : req.body.totalAmount
		};
		product.orders.push(orderer);

		// save
		return product.save()
		.then(product => true)
		.catch(err => err.message);
	});

	// check if product is updated
	if (isProductUpdated !== true) {
		return res.send({message : isProductUpdated});
	}

	if (isUserUpdated && isProductUpdated) {
		return res.send({message : 'Order Successful!'});
	}
};




//Route for Getting User's Orders.
module.exports.getMyOrders = (req, res) => {

	User.findById(req.user.id)
	.then(user => {
		console.log(user);
		return res.send(user.orders);
	})
	.catch(err => res.send(err));
};


// Get All Orders (Admin Only)

module.exports.getAllOrders = (req, res) => {

	User.find({})
	.then(allUsers => {

		// log info to check
		console.log(allUsers);
		console.log(allUsers.length);

		allOrders = []

		for (let i = 0; i < allUsers.length; i++) {
			console.log(`\n${i}\n`);
			console.log(allUsers[i]);
			if (allUsers[i].orders.length > 0) {
				allOrders.push(allUsers[i].email);
				allOrders.push(allUsers[i].orders);
			}
		};
		
		res.send(allOrders);
	})
	.catch(err => res.send(err));
};








