const Product = require("../models/Product");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//For Adding a New Product.
module.exports.addNewProduct = (req, res) => {

	console.log(req.body);

	Product.find({})
	.then(result => {

				let newProduct = new Product({
					name : req.body.name,
					description : req.body.description,
					price : req.body.price
				});

				newProduct.save()
				.then(product => res.send(product))
				.catch(err => res.send(err));
	
	})
	.catch(err => res.send(err));
};



//For Retrieving All Products.
module.exports.getAllProducts = (req, res) => {

	Product.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


//For Retrieving All Active Products.
module.exports.getActiveProducts = (req,res) => {

	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


//For Retrieving a Specific Product.
module.exports.getSingleProduct = (req, res) => {

	// log info to check
	console.log(req.params.id)
	console.log(req.user);

	// find by id
	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};



//For Updating a Product.
module.exports.updateProduct = (req, res) => {

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price		
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));
};

//Archive a Product.
module.exports.archiveProduct = (req, res) => {
	console.log(req.params.id);
	let updates = {
		isActive: false
	}
	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(error => res.send(error))
};


//For Activating a Product
module.exports.activateProduct = (req, res) => {
	console.log(req.params.id);
	let updates = {
		isActive: true
	}
	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(error => res.send(error))
};

//For Deleting a Product.
module.exports.deleteProduct = (req, res) => {
	console.log(req.params.id)
	Product.deleteOne({_id: req.params.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};









