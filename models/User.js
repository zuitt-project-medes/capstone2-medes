const mongoose = require('mongoose');

let userSchema = new mongoose.Schema(
	{
		userName: {
			type: String,
			required: [true, "User Name is required."]
		},
		firstName: {
			type: String,
			required: [true, "First Name is required."]
		},

		lastName: {
			type: String,
			required: [true, "Last Name is required."]
		},

		email: {
			type: String,
			required: [true, "Email is required."]

		},

		password: {
			type: String,
			required: [true, "Password is required."]
		},

		mobileNo: {
			type: String,
			required: [true, "Mobile Number is required."]
		},

		isAdmin: {
			type: Boolean,
			default: false
		},
		orders : [
		{
			userId : {
				type : String,
				required : [true, "User ID is required."]
			},

			productId : {
				type : String,
				required : [true, "Product ID is required."]
			},

			totalAmount : {
				type : Number,
				required : [true, "Amount ordered is required."]
			},

			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]


	},
);

module.exports = mongoose.model("User", userSchema);
