const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;


//Route for Signing Up/ Registration.
router.post("/registerUser", userControllers.registerUser);


//Route for Checking if Email Exists.
router.post("/checkEmailExists", userControllers.checkEmailExists);


//Route for Regular User Log-In.
router.post("/login", userControllers.loginUser);


//Route for Retrieving User Details.
router.get("/getUserDetails/:id", verify, userControllers.getUserDetails);


// Route for Updating a Regular User to Admin.
router.put("/updateUserAsAdmin/:id", verify, verifyAdmin, userControllers.updateUserAsAdmin);


// Route for Updating an Admin to User.
router.put("/updateAdminToUser/:id", verify, verifyAdmin, userControllers.updateAdminToUser);


//Route for Retrieving All Users (Admin Only).
router.get("/getAllUsers", verify, verifyAdmin, userControllers.getAllUsers);



//Route for Creating an Order (Regular User Only).
router.post("/createOrder", verify, verifyAdmin, userControllers.createOrder);

//Route for Getting User's Orders.
router.get("/getMyOrders", verify, userControllers.getMyOrders);

// Get All Orders (Admin Only)
router.get("/getAllOrders", verify, verifyAdmin, userControllers.getAllOrders);


module.exports = router;
